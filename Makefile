bsh: 
	gcc -Wall -Wextra -Wshadow -Wunreachable-code -pedantic bullsh.c -o bullsh

redirect:
	gcc redirecttest.c -o redirecttest

testers: redirect

all: bsh redirect

memtest: bsh
	valgrind --trace-children=yes --leak-check=yes --leak-check=full --show-leak-kinds=all ./bullsh
