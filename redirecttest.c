#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>

int main () {
	int pid = fork();
	if(pid == 0) {
		int fd = open("redirect.txt", O_WRONLY|O_CREAT, 00666);
		dup2(fd, STDOUT_FILENO);
		printf("Output 1\n");
		close(fd);
		exit(0);
	}
	printf("Output 2\n");
	return 0;
}
