#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

typedef int BOOL;

#define TRUE 1
#define FALSE 0

//For 'method' argument of remove_background_job
#define BY_JOBID 0x0
#define BY_PID 0x1
#define ID_MASK 0x1
#define KILL 0x2

#define BLOCK_SIZE 16384

/**
 * Returns size of file
 * @param path Filepath
 * @return Size of file at path
 */
off_t get_size(const char *path) {
	struct stat s;
	return stat(path, &s) == 0 ? s.st_size : -1;
}

//Needed for parsing data with getdents
struct linux_dirent {
	long			d_ino;
	off_t			d_off;
	unsigned short	d_reclen;
	char			d_name[];
};

//Structure for storing background job data
struct job {
	int		job_id;
	char	*job_name;
	int		pid;
};



////LINKED LIST OF BACKGROUND JOBS////
//Node of Singly Linked List
struct jobnode {
	struct job *data;
	struct jobnode *next;
};

/**
 * Wrapper for creating new job node
 * @param name Command that instantiates this job
 * @param pid PID of the job
 * @param *jc Pointer to a job id counter
 * @return New struct jobnode
 */
struct jobnode *create_job_node(char *name, int pid, int *jc) {
	struct jobnode *jn = (struct jobnode *)malloc(sizeof(struct jobnode));
	struct job *j = (struct job *)malloc(sizeof(struct job));
	j->job_id = *jc;
	j->job_name = name;
	j->pid = pid;
	jn->data = j;
	jn->next = NULL;
	*jc += 1; //Increment the job counter so the next job gets a unique id
	return jn;
}

//List structure for tracking length and head of the list.
struct joblist {
	unsigned int	length;
	struct jobnode	*head;
};

/**
 * Wrapper for creating new Linked List
 * @return New empty linked list
 */
struct joblist *create_job_list() {
	struct joblist *jl = (struct joblist *)malloc(sizeof(struct joblist));
	jl->length = 0;
	jl->head = NULL;
	return jl;
}
/////////////////////////////////////////

/**
 * GLOBAL VARIABLES
 */
struct joblist *job_list; //Global linked list of jobs that can be passed to signal handlers
int m_pid = 0; //Global PID, stores the pid of the last child

/**
 * Wrapper to add a background job to a linked list
 * @param *list The linked list to prepend to
 * @param *name The name of the job
 * @param pid The PID of the child
 * @param *jc A pointer to the job counter
 * @return The job id of the new job
 */
int add_background_job(struct joblist *list, char *name, int pid, int *jc) {
	struct jobnode *j = create_job_node(name, pid, jc);
	if(j == NULL) {
		printf("[error] Could not add background job\n");
		return -1;
	}
	j->next = list->head;
	list->head = j;
	list->length++;
	return j->data->job_id;
}

/**
 * Helper function to remove a node from a linked list
 * @param *list The linked list to remove from
 * @param id Identifier for the node to remove (either PID or job id)
 * @param method Constant that represents whether id is a PID or job id (should be either BY_PID or BY_JOBID and bitwise OR'd with KILL if the background job was killed)
 * @return The PID of the deleted job
 */
int remove_background_job(struct joblist *list, int id, int method) {
	if(list->length == 0) return -1;
	struct jobnode *iterator = list->head;
	//Compare the id param to the head's job_id or pid, depending on the method param
	if((iterator->data->pid == id && (method&ID_MASK) == BY_PID) || (iterator->data->job_id == id && (method&ID_MASK) == BY_JOBID)) {
		int pid = iterator->data->pid;
		list->length--;
		if(list->length == 0) {
			list->head = NULL;
			if(method&KILL) printf("%d: Done\t%s\n", iterator->data->job_id, iterator->data->job_name);
			free(iterator->data->job_name);
			free(iterator->data);
			free(iterator);
		}
		else {
			list->head = iterator->next;
			if(method&KILL) printf("%d: Done\t%s\n", iterator->data->job_id, iterator->data->job_name);
			free(iterator->data->job_name);
			free(iterator->data);
			free(iterator);
		}
		return pid;
	}
	//Iterate through the linked list, with reference to both the current value of the iterator and its neighbour
	while(list->length > 0 && iterator->next != NULL) {
		//Compare the id param to the iterator's job_id or pid, depending on the method param
		if((iterator->next->data->pid == id && (method&ID_MASK) == BY_PID) || (iterator->next->data->job_id == id && (method&ID_MASK) == BY_JOBID)) {
			int pid = iterator->next->data->pid;
			if(method&KILL) printf("%d: Done\t%s\n", iterator->data->job_id, iterator->data->job_name);
			free(iterator->next->data->job_name);
			free(iterator->next->data);
			free(iterator->next);
			iterator->next = iterator->next->next;
			list->length--;
			return pid;
		}
		iterator = iterator->next;
	}
	return -1; //This will only be called if the id isn't found
}

/**
 * Signal handler for SIGCHLD
 * Removes BG jobs from jobs_list as soon as they're killed
 */
void childHandler() {
	pid_t pid;
	int status;
	while((pid = waitpid(-1, &status, WNOHANG)) > 0) {
		kill(pid, SIGTERM);
		remove_background_job(job_list, pid, BY_PID|KILL);
	}
}

/**
 * Signal handler for SIGINT
 * Kills the job associated with the most recent PID, unless it's a BG job
 */
void ctrlcHandler() {
	struct jobnode *iterator = job_list->head;
	while(iterator != NULL) {
		if(iterator->data->pid == m_pid) {
			return;
		}
		iterator = iterator->next;
	}
	if(m_pid != 0) {
		kill(m_pid, SIGKILL);
	}
}

/**
 * Retrieves a command line from the user
 * @param *prompt The prompt string
 * @param *args An array to store the command tokens
 * @param *background Pointer to a BOOL that determines a background job
 * @param *redirect_file Pointer to a string that will store the path of a potential redirect file
 * @return The amount of tokens
 */
int get_cmd(char *prompt, char *args[], BOOL *background, char *redirect_file) {
	int length, i = 0;
	char *line = NULL;
	char *token, *loc;
	size_t linecap = 0;
	printf("%s ", prompt);
	length = getline(&line, &linecap, stdin);
	if(length <= 0) {
		free(line);
		exit(-1);
	}

	if((loc = index(line, '&')) != NULL) {
		*background = TRUE;
		*loc = ' ';
	} else {
		*background = FALSE;
	}

	int c = 0;
	if((loc = index(line, '>')) != NULL) {
		*loc =  ' ';
		loc++;
		for(; *loc <= 32; loc++); //Forward to next alphanumeric character
		for(; *loc > 32; loc++) { //Transfer redirect file path to *redirect_file and remove traces from line
			redirect_file[c++] = *loc;
			*loc = ' ';
		}
		redirect_file[c] = '\0';
	}
	else {
		*redirect_file = 0;
	}

	char *line2 = line; //To avoid memory leaks from strsep

	while((token = strsep(&line2, " \t\n")) != NULL) {
		for(unsigned int j = 0; j < strlen(token); j++) {
			if(token[j] <= 32) {
				token[j] = '\0';
			}
		}
		if(strlen(token) > 0) {
			args[i++] = strdup(token); //Use strdup to safely free line
		}
	}
	free(line);
	return i;
}

/**
 * Built-in cat command
 * @param *path The file to output
 * @return Error code from read syscall
 */
int do_cat(char *path) {
	int fd = open(path, O_RDONLY, 00222);
	if(fd == -1) {
		perror("[error]");
		return errno;
	}

	off_t source_size = get_size(path);
	if (source_size != -1) {
		char *buf = (char *)malloc(source_size + 1); //Store extra bit for null byte
		memset(buf,0,source_size + 1);
		int out = 0;
		while((out = read(fd, buf, source_size)) > 0);
		close(fd);
		printf("%s\n", buf);
		free(buf);
	}
	else {
		perror("[error] Could not determine file size");
	}
	return errno;
}

/**
 * Built-in cp command
 * @param src Source file
 * @param dst Destination file
 * @return Error code from read or write
 */
int do_cp(char *src, char *dst) {
	int fd_src = open(src, O_RDONLY, 00222);
	if(fd_src == -1) {
		perror("[error]");
		return errno;
	}
	int fd_dst = open(dst, O_WRONLY|O_CREAT, 00666);
	if(fd_dst == -1) {
		perror("[error]");
		return errno;
	}
	off_t source_size;
	if((source_size = get_size(src)) != -1) {
		char *buf = (char *)malloc(source_size);
		memset(buf, 0, source_size);
		int out = 0;
		while((out = read(fd_src, buf, source_size)) > 0) {
			write(fd_dst, buf, source_size);
		}
		write(fd_dst, "\n", 1); //To avoid NOEOL
		close(fd_src);
		close(fd_dst);
		free(buf);
	}
	else {
		perror("[error] Could not determine file size");
	}
	return errno;
}

/**
 * Built-in ls command
 * @param *dir Directory to list. NULL or the empty string represent the current working directory
 * @return errno
 */
int do_ls(char *dir) {
	int fd;
	if(dir == NULL || strlen(dir) == 0) {
		fd = openat(AT_FDCWD, ".", O_RDONLY|O_DIRECTORY); //'.' represents current working directory
	}
	else {
		fd = open(dir, O_RDONLY|O_DIRECTORY);
		printf("%s:\n", dir);
	}
	if(fd == -1) {
		perror("[error]");
		return errno;
	}
	char buf[BLOCK_SIZE]; //Create sufficiently large buffer for file list
	struct linux_dirent *dirent;
	int nread = 0;
	char d_type;
	
	while((nread = syscall(SYS_getdents, fd, buf, BLOCK_SIZE)) != 0) { //Get linux_dirents for files in *dir
		for(int c = 0; c < nread;) {
			dirent = (struct linux_dirent*)(buf + c); //Cast buf + c to a linux_dirent
			d_type = *(buf + c + dirent->d_reclen - 1); //Calculate position of d_type member
			switch(d_type) {
				case DT_DIR:
					printf("\x1b[1m%s\x1b[0m\n", dirent->d_name); //Print bold for directories
					break;
				case DT_LNK:
					printf("\x1b[35m%s\x1b[0m\n", dirent->d_name); //Print in color for symlinks
					break;
				default:
					printf("%s\n", dirent->d_name);
					break;
			}
			c += dirent->d_reclen; //Increment c to point to the beginning of the next linux_dirent
		}
	}
	return 0;
}

int main() {
	int job_counter = 1;
	signal(SIGCHLD, childHandler);
	signal(SIGINT, ctrlcHandler);
	signal(SIGTSTP, SIG_IGN); //Just ignore SIGSTP

	//Initializing stuff for random number generation
	time_t now;
	srand((unsigned int) (time(&now)));

	job_list = create_job_list();
	char *args[20];
	char redirect[20];
	BOOL bg;
	int argc = 0;

	while(TRUE) {
		bg = FALSE;
		//Clear args each time to avoid running into problems when freeing args
		for(int c = 0; c < argc; c++) {
			free(args[c]);
			args[c] = NULL;
		}
		argc = get_cmd("\x1b[1m\x1b[31mbullsh\x1b[39m >\x1b[0m", args, &bg, redirect); //Beautiful prompt
		if(argc == 0) {
			continue;
		}

		args[argc] = 0; //Null-terminate the args list

		if(strcmp(args[0], "exit") == 0) { //Clean up and exit the shell
			free(job_list);
			for(int c = 0; c < argc; c++) {
				free(args[c]);
				args[c] = NULL;
			}
			exit(0);
		} 
		else if(strcmp(args[0], "cd") == 0) {
			char *path;
			if(argc < 2) { //cd should default to switching to the user's home directory
				path = getenv("HOME");
			}
			else {
				path = args[1];
			}
			int r = chdir(path);
			if(r != 0) {
				perror("[error]");
			}
		}
		else if(strcmp(args[0], "fg") == 0) {
			if(argc < 2) {
				printf("[error] Usage: fg <job_id>\n");
				continue;
			}
			int job_id = atoi(args[1]);
			int npid; //To store pid of the job being foregrounded
			if((npid = remove_background_job(job_list, job_id, BY_JOBID)) == -1) {
				printf("[error] Job %d doesn't exist\n", job_id);
			}
			else {
				m_pid = npid;
				waitpid(npid, 0, 0); //Wait for the job to complete, thus foregrounding the job
			}
		}
		else {
			m_pid = fork();
			if(m_pid == 0) { //Child process
				int fd;
				int retval = 0;

				if(strlen(redirect) > 0) {
					fd = open(redirect, O_WRONLY|O_CREAT, 00666);
					if(fd != -1) dup2(fd, STDOUT_FILENO); //Write from stdout to redirect file. This dup is only valid for a single child
					else {
						perror("Error opening redirection file");
						exit(1);
					}
				}

				if(strcmp(args[0], "cat") == 0) {
					for(int c = 1; c < argc; c++) { //Ability to cat multiple files
						if(args[c][0] != '-') retval = do_cat(args[c]); //Avoid running into problems if someone tries passing flags
						printf("\n");
					}
				}
				else if(strcmp(args[0], "cp") == 0) {
					if(argc != 3) {
						printf("[error] Usage: cp file1 file2\n");
						exit(1);
					}
					retval = do_cp(args[1], args[2]);
				}
				else if(strcmp(args[0], "ls") == 0) {
					if(argc == 1) {
						retval = do_ls(""); //If no additional arguments are passed, pass empty string (for current directory)
					}
					else {
						for(int c = 1; c < argc; c++) { //Ability to ls multiple directories
							if(args[c][0] != '-') retval = do_ls(args[c]); //Avoid running into problems if someone tries passing flags
							printf("\n");
						}
					}
				}
				else if(strcmp(args[0], "jobs") == 0) {
					if(job_list->length == 0) {
						printf("There are currently no jobs running in the background\n");
						exit(0);
					}
					printf("Jobs running in background:\n");
					struct jobnode *iterator = job_list->head;
					while(iterator != NULL) {
						printf("[%d] %d\t%s\n", iterator->data->job_id, iterator->data->pid, iterator->data->job_name);
						iterator = iterator->next;
					}
					exit(0);
				}
				else {
					if(bg == TRUE) {
						signal(SIGINT, SIG_IGN); //Ignore SIGINT from child, if ommitted bg jobs may be killed unintentionally
					}
					retval = execvp(args[0], args);
					perror("[DEBUG] execvp error");
				}

				if(strlen(redirect) > 0) {
					close(fd);
				}

				int w, rem;
				w = rand() % 10;

				//Sleep after calling job to accomodate testing bg jobs
				rem = sleep(w);
				while(rem != 0) {
					rem = sleep(rem);
				}

				exit(retval);
			} else if(m_pid == -1) {
				printf("[error] Couldn't fork a process\n");
			} else {
				if(bg == FALSE) {
					waitpid(m_pid, 0, 0);
				}
				else {
					//Add background job when a new command is entered with &
					add_background_job(job_list, strdup(args[0]), m_pid, &job_counter);
				}
			}
		}
	}
	free(job_list);
	return 0;
}

